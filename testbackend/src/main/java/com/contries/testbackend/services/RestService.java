package com.contries.testbackend.services;

import com.contries.testbackend.Entities.Log;
import com.contries.testbackend.Repositories.LogRepository;
import com.contries.testbackend.models.ResponseModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Service
public class RestService {

    @Autowired
    private LogRepository logRepository;

    public  ResponseModel[] GetPaises() {
        try {
            URL url = new URL("https://restcountries.com/v3.1/all");
            try {
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                //con.setRequestProperty("accept", "application/json");
                con.setRequestMethod("GET");
                InputStream responseStream = con.getInputStream();
                ObjectMapper mapper = new ObjectMapper();
                Object[] apod = mapper.readValue(responseStream, Object[].class);
                ResponseModel[] data = new ResponseModel[apod.length];

                for (int i = 0; i < apod.length; i++){

                    JSONObject Obj0 = new JSONObject();
                    Obj0.put("data",apod[i]);
                    //System.out.println(Obj0.toString());
                    String str = Obj0.toString();
                    JSONObject Obj = new JSONObject(str);
                    String name =  Obj.getJSONObject("data").getJSONObject("name").getString("common");
                    String official =  Obj.getJSONObject("data").getJSONObject("name").getString("official");
                    String region =  Obj.getJSONObject("data").getString("region");
                    String subregion = Obj.getJSONObject("data").has("subregion") ? Obj.getJSONObject("data").getString("subregion"):null;

                    data[i] = new ResponseModel(name,official,region,subregion);

                }

                return  data;

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return new ResponseModel[0];
    }


    public  ResponseModel[] GetPaisesByRegion(String regionName) {
        try {
            URL url = new URL("https://restcountries.com/v3.1/region/".concat(regionName));
            try {
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                //con.setRequestProperty("accept", "application/json");
                con.setRequestMethod("GET");
                InputStream responseStream = con.getInputStream();
                ObjectMapper mapper = new ObjectMapper();
                Object[] apod = mapper.readValue(responseStream, Object[].class);
                ResponseModel[] data = new ResponseModel[apod.length];

                for (int i = 0; i < apod.length; i++){

                    JSONObject Obj0 = new JSONObject();
                    Obj0.put("data",apod[i]);
                    //System.out.println(Obj0.toString());
                    String str = Obj0.toString();
                    JSONObject Obj = new JSONObject(str);
                    String name =  Obj.getJSONObject("data").getJSONObject("name").getString("common");
                    String official =  Obj.getJSONObject("data").getJSONObject("name").getString("official");
                    String region =  Obj.getJSONObject("data").getString("region");
                    String subregion = Obj.getJSONObject("data").has("subregion") ? Obj.getJSONObject("data").getString("subregion"):null;

                    data[i] = new ResponseModel(name,official,region,subregion);

                }

                return  data;

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return new ResponseModel[0];
    }

    public  void  SaveLog(String fecha,String hora,String endpoint,String ip) {
        Log log = new Log();
        log.Setdata(fecha,hora,endpoint,ip);
        this.logRepository.save(log);
    }


}
