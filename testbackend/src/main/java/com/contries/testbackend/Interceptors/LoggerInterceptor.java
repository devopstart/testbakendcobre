package com.contries.testbackend.Interceptors;


import com.contries.testbackend.Repositories.LogRepository;
import com.contries.testbackend.services.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class LoggerInterceptor implements HandlerInterceptor {

    @Autowired
   private RestService restService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String fecha = dateFormat.format(date);
        String time = timeFormat.format(date);
        String endpoint = request.getRequestURI();
        String ip = request.getRemoteAddr();
        System.out.println("Fecha: " + fecha);
        System.out.println("Hora: " + time);
        System.out.println("Endpoint: " + request.getRequestURI());
        System.out.println("Ip: " + request.getRemoteAddr());
        this.restService.SaveLog(fecha,time,endpoint,ip);
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
