package com.contries.testbackend.Configurations;

import com.contries.testbackend.Interceptors.LoggerInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class InterceptorConfiguration extends WebMvcConfigurationSupport{

    @Autowired
    LoggerInterceptor loggerInterceptor;

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
            registry.addInterceptor(loggerInterceptor).addPathPatterns("/**");
            //super.addInterceptors(registry);
    }

}
