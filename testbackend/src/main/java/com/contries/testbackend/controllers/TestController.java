package com.contries.testbackend.controllers;

import com.contries.testbackend.models.ResponseModel;
import com.contries.testbackend.services.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/")   ///prefijo a todas las rutas fijas que colocamos en el controlador
public class TestController {

    @Autowired
    RestService restService;


    @GetMapping("/countries")
    public ResponseModel[] paises() {
        return this.restService.GetPaises();
    }

    @GetMapping("/countries/{regionName}")
    public ResponseModel[] paisesByregion(@PathVariable("regionName") String regionName) {
        return this.restService.GetPaisesByRegion(regionName);

    }



}
