package com.contries.testbackend.Entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "logs")
public class Log {

    public void Setdata(String fecha,String hora,String endpoint,String ip) {
        this.fecha = fecha;
        this.hora = hora;
        this.endpoint = endpoint;
        this.ip = ip;
    }

    @Id
    @GeneratedValue
    private Long id;
    private String fecha;
    private String hora;
    private String endpoint;
    private String ip;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
